
const express = require('express'),
    http = require('http'),
    app = express(),
    server = http.createServer(app),
    io = require('socket.io').listen(server);
app.get('/', (req, res) => {

    res.send('Chat Server is running')
});
io.on('connection', (socket) => {
    socket.on('join', function (userNickname) {
        socket.broadcast.emit('userjoinedthechat', userNickname + " : has joined the chat ");
    })


    socket.on('messagedetection', (senderNickname, messageContent) => {
        socket.broadcast.emit('emojirain', "12345");
    })

    socket.on('emojirain', (senderNickname, messageContent) => {
        let message = { "message": messageContent, "senderNickname": senderNickname }
        io.emit('message', "12345")
    })

    socket.on('disconnect', function () {
        socket.broadcast.emit("userdisconnect", ' user has left')
    })
})

server.listen(3000, () => {

    console.log('Node app is running on port 3000')

})
